package de.namnodorel.ardacraft.gates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class GateManager {
	
	private static HashMap<String, GateEntry> gates = new HashMap<>();
	private static HashMap<String, GateEntry> selectedGates = new HashMap<>();
	private static List<String> gatesWhichmMayBeDeleted = new ArrayList<>();
	
	public static HashMap<String, GateEntry> getGates(){
		return gates;
	}
	
	public static void setGates(HashMap<String, GateEntry> newgates){
		gates = newgates;
	}
	
	public static void addGate(GateEntry g){
		gates.put(g.getName(), g);
	}
	
	public static void removeGate(String name){
		gates.remove(name);
	}
	
	public static GateEntry getGateByName(String name){
		return gates.get(name);
	}
	
	public static boolean isFrameValid(Area frame, Area gate){
		
		if(frame.getXDimension() != gate.getXDimension() || frame.getYDimension() != gate.getYDimension() || frame.getZDimension() != gate.getZDimension()){
			return false;
		}
		
		return true;
	}
	
	public static boolean isInsideAnyGate(Location loc){
		
		for(String key : gates.keySet()){
			
			if(gates.get(key).getArea().isInside(loc)){
				return true;
			}
			
		}
		
		return false;
	}
	
	public static GateEntry getGateByLocation(Location loc){

		GateEntry g = null;
		
		for(String key : gates.keySet()){
			
			g = gates.get(key);
			
			if(!g.getArea().isInside(loc)){
				continue;
			}else{
				break;
			}
			
		}
		
		return g;
	}
	
	public static GateEntry getNearGate(Location loc){
		
		for(String key : gates.keySet()){
			
			if(isNearBy(loc, gates.get(key))){
				return gates.get(key);
			}
			
		}
		
		
		return null;
	}
	
	private static boolean isNearBy(Location loc, GateEntry g){
		
		if(g.getArea().getFirstLocation().distance(loc) < 5 || g.getArea().getFirstLocation().distance(loc) < 5){
			return true;
		}
		
		return false;
	}

	public static boolean existsGateWithName(String name) {
		return gates.containsKey(name);
	}

	public static void setSelected(String player, GateEntry gate) {
		selectedGates.put(player, gate);
	}
	
	public static boolean hasSelected(String name){
		return selectedGates.containsKey(name);
	}
	
	public static GateEntry getSelected(String name){
		return selectedGates.get(name);
	}
	
	public static void removeSelection(String gate){
		for(String key : selectedGates.keySet()){
			
			if(selectedGates.get(key).getName().equalsIgnoreCase(gate)){
				selectedGates.remove(gate);
			}
			
		}
	}
	
	public static void addGateWhichMayBeDeleted(String gate){
		gatesWhichmMayBeDeleted.add(gate);
	}
	
	public static boolean canBeDeleted(String gate){
		return gatesWhichmMayBeDeleted.contains(gate);
	}
	
	public static void removeGateWhichMayBeDeleted(String gate){
		gatesWhichmMayBeDeleted.remove(gate);
	}
	
	//Checks wether or not the player is near a gate (and if it should be opened)
	public static void handleAutoGateOpening(Player p){
		
		GateEntry g = GateManager.getNearGate(p.getLocation());

		//If there is no gate in range, return
		if(g == null){
			return;
		}

		//If the gate is in any other mode than auto, return
		if(!(g.getMode() == GateMode.AUTO_EVERYONE || g.getMode() == GateMode.AUTO_PERMISSION)){
			return;
		}

		//If a permission is needed which the player doesn't have, return
		if(g.getMode() == GateMode.AUTO_PERMISSION && !p.hasPermission("de.namnodorel.ardacraft.gates.interact." + g.getName())){
			return;
		}

		//If the gate is moving, return
		if(g.isMoving()){
			return;
		}

		//Open the gate
		g.open();
		
		//Run the scheduler to close the gate after five seconds
		Bukkit.getScheduler().scheduleSyncDelayedTask(Gates.getPlugin(), new Runnable(){

			@Override
			public void run() {
				g.close();
				
			}
			
			//Wait, until the gate has opened (g.getWaitMS()*g.getFrames().size()) and then wait 5 seconds before closing the gate (5*20L)
		}, g.getWaitMS()*g.getFrames().size() + g.getPauseMS());

		
	}
}
