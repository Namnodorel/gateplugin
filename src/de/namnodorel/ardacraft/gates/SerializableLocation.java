package de.namnodorel.ardacraft.gates;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SerializableLocation implements Serializable{
	
	private Integer X;
	private Integer Y;
	private Integer Z;
	private String world;
	
	public Integer getX() {
		return X;
	}
	public SerializableLocation setX(Integer x) {
		X = x;
		return this;
	}
	public Integer getY() {
		return Y;
	}
	
	public SerializableLocation setY(Integer y) {
		Y = y;
		return this;
	}
	public Integer getZ() {
		return Z;
	}
	public SerializableLocation setZ(Integer z) {
		Z = z;
		return this;
	}
	public String getWorld() {
		return world;
	}
	public SerializableLocation setWorld(String world) {
		this.world = world;
		return this;
	}
	
}
