package de.namnodorel.ardacraft.gates;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Gates extends JavaPlugin{
	
	private static Gates plugin;
	
	@Override
	public void onEnable(){
		
		plugin = this;
		
		Data.loadAll();
		
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(new GlobalListener(), this);
		
		this.getCommand("gate").setExecutor(new GateCommand());
		
		//Check 1 time per second, if any Player is near to a gate
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				
				for(Player p : Bukkit.getServer().getOnlinePlayers()){
					GateManager.handleAutoGateOpening(p);
				}
				
			}
			
			
		}, 20L, 20L);
		
		//Save gates all 10 minutes
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				System.out.println("Saving gates...");
				Data.saveAll();
				
			}
			
			
		}, 20L*60*20, 20L*60*10);
	}
	
	@Override
	public void onDisable(){
		
		Data.saveAll();
		
	}
	
	public static Gates getPlugin(){
		return plugin;
	}
}
